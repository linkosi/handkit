#!/bin/bash

image_name="handkit"
docker build --rm -t $image_name .
docker run --env-file ./.env -v $(pwd)/data:/app/data -it $image_name python main.py 
