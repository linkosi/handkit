# Handkit
This bot is used for management of groups, getting backups and whatever else one would need!

## Setup
This bot was developed using Python 3.9, minimum requirement is probably Python 3.6.

```sh
git clone https://gitlab.com/linkosi/handkit.git 
cd handkit
python3 -m venv .venv && source .venv/bin/activate
pip3 install -r ./requirements.txt
python3 main.py
```
you will need a valid Telegram client to setup pyrogram.
once you are done, copy `main.conf.json.example` file to `main.conf.json` and fill it in.
