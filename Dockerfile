FROM python:3.10-alpine as build-env

COPY . /app
WORKDIR /app

RUN apk add --no-cache gcc musl-dev

RUN pip install -r ./requirements.txt

CMD python main.py