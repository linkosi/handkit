from os import getenv
from dotenv import load_dotenv

load_dotenv()

class Config:

    PRIVATE : list
    NON_PRIVATE: list
    GROUP: list
    SUPER_GROUP: list
    CHANNEL: list
    M_BACKUP: str
    PRIVATE_BACKUP: str
    L_STREAM: list
    VALID_RTMP_CHATS: list

    def __init__(this):
        this.PRIVATE = this.list_property('PRIVATE_CHAT_IDS')
        this.NON_PRIVATE = this.list_property('NON_PRIVATE')
        this.GROUP = this.list_property('GROUP')
        this.SUPER_GROUP = this.list_property('SUPERGROUP')
        this.CHANNEL = this.list_property('CHANNEL')
        this.M_BACKUP = getenv('M_BACKUP')
        this.PRIVATE_BACKUP = getenv('PRIVATE_BACKUP')
        this.L_STREAM = this.list_property('L_STREAM')
        this.VALID_RTMP_CHATS = this.list_property('VALID_RTMP_CHATS')

    def list_property(self, name: str):
        return getenv(name).split(',')