import os
from time import time
from pyrogram import Client
from pyrogram.raw.functions.phone import GetGroupCallStreamRtmpUrl, CreateGroupCall, DiscardGroupCall, StartScheduledGroupCall
from pyrogram.raw.types import UpdateNewChannelMessage
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from config import Config


app = Client("account", api_id=os.getenv("PYROGRAM_API_ID"), api_hash=os.getenv("PYROGRAM_API_HASH"), workdir="data")


scheduler = AsyncIOScheduler()

conf = Config()

# Forwarder
PRIVATE = conf.PRIVATE
NON_PRIVATE = conf.NON_PRIVATE
GROUP = conf.GROUP
SUPER_GROUP = conf.SUPER_GROUP
CHANNEL = conf.CHANNEL
M_BACKUP = conf.M_BACKUP
PRIVATE_BACKUP = conf.PRIVATE_BACKUP
L_STREAM = conf.L_STREAM

# RTMP
VALID_RTMP_CHATS = conf.VALID_RTMP_CHATS
CALLS = {}
PENDING_STREAMS = {}

async def get_rmtp_key_and_url(c, message):
    if message.chat.id not in VALID_RTMP_CHATS or not message.text.startswith('/rtmp'):
        return
    user = await app.get_chat_member(message.chat.id, message.from_user.id)
    if user.status not in ['administrator', 'creator']:
        await message.delete()
        return
    peer_ = await c.resolve_peer(message.chat.id)
    token = await c.send(GetGroupCallStreamRtmpUrl(peer=peer_, revoke=False))
    await app.send_message(message.from_user.id, f"`{token.url}`\n`{token.key}`", parse_mode="markdown")
    await app.send_message("me", f"@{message.from_user.username} requested a token.\n#RTMP")
    if(not message.text.startswith('/rtmp_token')):
        await c.send(CreateGroupCall(peer=peer_, random_id=int(time()), rtmp_stream=True))
    await message.delete()


async def schedule_group_stream(c, message):
    if message.chat.id not in VALID_RTMP_CHATS or not message.text.startswith('/stream'):
        return
    user = await app.get_chat_member(message.chat.id, message.from_user.id)
    if user.status not in ['administrator', 'creator']:
        await message.delete()
        return
    peer_ = await c.resolve_peer(message.chat.id)
    parsed_message = message.text.split(' ')
    # /stream Titanic 123213111
    if(len(parsed_message) >= 3 and parsed_message[-1].isdigit()):
        call = await c.send(CreateGroupCall(peer=peer_, random_id=int(time()), rtmp_stream=True, 
        schedule_date=int(parsed_message[-1]), 
        title=' '.join(parsed_message[1:-1])))
        PENDING_STREAMS[str(message.chat.id)] = int(parsed_message[-1])
    # /stream Titanic
    elif len(parsed_message) >= 2 and not parsed_message[-1].isdigit():
        call = await c.send(CreateGroupCall(peer=peer_, random_id=int(time()), rtmp_stream=True, 
        title=' '.join(parsed_message[1:])))
    # /stream
    else:
        call = await c.send(CreateGroupCall(peer=peer_, random_id=int(time()), rtmp_stream=True))
    await log_call_updates(call.updates, message)
    await message.delete()

async def log_call_updates(updates, message):
    for update in updates:
            if isinstance(update, UpdateNewChannelMessage):
                CALLS[str(message.chat.id) + "_call"] = update.message.action.call
                break

async def abort_group_call_command(client, message):
    if message.chat.id not in VALID_RTMP_CHATS or not message.text.startswith('/abort'):
        return
    user = await app.get_chat_member(message.chat.id, message.from_user.id)
    if user.status not in ['administrator', 'creator']:
        await message.delete()
        return
    
    if(str(message.chat.id) + "_call" in CALLS):
        await client.send(DiscardGroupCall(call=CALLS[str(message.chat.id) + "_call"]))
        CALLS.pop(str(message.chat.id) + "_call")
    else:
        await app.send_message(message.chat.id, '[AUTOMATED] Cannot abort stream, stream is not present in RAM.')
    await message.delete()

async def start_pending_stream():
    to_be_removed = []
    for chat_id, start_time in PENDING_STREAMS.items():
        if time() >= start_time:
            await app.send(StartScheduledGroupCall(call=CALLS[chat_id + '_call']))
            to_be_removed.append(chat_id)
    for chat_id in to_be_removed:
        PENDING_STREAMS.pop(chat_id)

async def forward_Messages(client, message):
    if message.chat.type == "private":
        if not message.from_user.is_self:
        # Sending all message from my private list in "M's Backup"
            if message.from_user.id in conf.PRIVATE:
                # Forward message to "M's Backup"
                await app.forward_messages(M_BACKUP, message.chat.id, message.message_id)
            # Sending all message form my private chat in "Private Backup"
            else:
                await app.forward_messages(PRIVATE_BACKUP, message.chat.id, message.message_id)
                # Getting last message
                last_Message = await app.get_history(PRIVATE_BACKUP, limit=1)
                # Sending message as reply
                buildMessage = f"Name: **{message.from_user.first_name}**\n"

                if message.from_user.last_name:
                    buildMessage += f"Last Name: **{message.from_user.last_name}**\n"

                if message.from_user.username:
                    buildMessage += f"Username: @{message.from_user.username}\n"

                buildMessage += f"ID: `{message.from_user.id}`"
                await app.send_message(PRIVATE_BACKUP, buildMessage, reply_to_message_id=last_Message[0].message_id, parse_mode='markdown')
        
    if message.chat.type == "group":
        if message.chat.id in GROUP:
            await app.forward_messages(PRIVATE_BACKUP, message.chat.id, message.message_id)
    
    if message.chat.type == "supergroup":
        if message.chat.id in SUPER_GROUP:
            await app.forward_messages(L_STREAM, message.chat.id, message.message_id)

    if message.chat.type == "channel":
        if message.chat.id in CHANNEL:
            await app.forward_messages(PRIVATE_BACKUP, message.chat.id, message.message_id)

async def send_details(chat, is_group):
    buildMessage = ""
    if is_group:
        buildMessage = f"Name: **{chat.title}**\n"
    else:
        buildMessage = f"Name: **{chat.first_name}**\n"

    if chat.last_name:
        buildMessage += f"Last Name: **{chat.last_name}**\n"

    if chat.username:
        buildMessage += f"Username: @{chat.username}\n"

    buildMessage += f"ID: `{chat.id}`"
    await app.send_message("me", buildMessage, parse_mode='markdown')


async def get_id_command(message):
    if message.chat.type == "group":
        return

    if message.from_user and message.from_user.is_self:
        if not message.text.startswith('/getid'):
            return
        await message.delete()
    else:
        return

    chat = None

    if message.reply_to_message and message.reply_to_message.forward_from:
        chat = message.reply_to_message.forward_from
        
    elif message.reply_to_message:
        chat = message.reply_to_message.from_user  

    else:
        chat = message.chat

    is_group = message.chat.type != "private" and not message.reply_to_message
    await send_details(chat, is_group)


@app.on_message()
async def on_message(client, message):
    "Get an incoming message and pass it onto handler functions, there was a problem when we tried to add multiple handlers"
    if(not hasattr(message, 'text')): # We are only dealing with text messages so ignore everything else
        return
    await forward_Messages(client, message)
    await get_rmtp_key_and_url(client, message)
    await schedule_group_stream(client, message)
    await get_id_command(message)
    await abort_group_call_command(client, message)

scheduler.add_job(start_pending_stream, "interval", seconds=3)
scheduler.start()
app.run()
